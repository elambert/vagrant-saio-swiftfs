vagrant-swift-all-in-one
========================

A Swift-All-In-One in a few easy steps.

 1. `vagrant up`
 1. `vagrant ssh`
 1. `echo "awesome" > test`
 1. `swift upload test test`
 1. `swift download test test -o -`

This project assumes you have Virtualbox and Vagrant.

 * https://www.virtualbox.org/wiki/Downloads
 * http://www.vagrantup.com/downloads.html

running-tests
=============

You should be able to run most tests without too much fuss.

 1. `.unittests`
 1. `.functests`
 1. `.probetests`
 1. `vtox -e pep8`
 1. `vtox -e py27`
 1. `vtox -e py26`
 1. `vtox  # run all gate checks`

localrc-template
========================

A few things are configurable, see `localrc-template`.

 1. `cp localrc-template localrc`
 1. `vi localrc`
 1. `source localrc`
 1. `vagrant provision`
 1. `vagrant ssh`
 1. `rebuildswift`

swift-fs
========================
 1. `vagrant up`
 1. `vagrant ssh`
 1. `git clone https://github.com/wizzard/SwiftFS.git`
 1. `./autogen.sh && PKG_CONFIG_PATH=/opt/libevent2.1/lib/pkgconfig ./configure && make && sudo make install`
 1. make mount point
    `mkdir ~/mnt`
    `chmod 777 ~/mnt`
 1. get auth-token and storage url
    `curl -X GET -i -H "X-Auth-User: test:tester" -H "X-Auth-Key: testing" http://127.0.0.1:8080/auth/v1.0`
 1. create container
    `curl -X PUT -i -H "X-Auth-Token: ${X-Auth-Token}"  ${X-Storage-URL}/${newcontainer}`
 1. turn off ssl
    `sudo vi /usr/local/etc/swiftfs/swiftfs.conf.xml`
    <ssl_enable_validation type="boolean">False</ssl_enable_validation>
 1. set auth environment variables
    `export export SwiftFS_USER="test:tester"`
    `export export SwiftFS_PWD="testing"`
 1. start swift-fs
    `sudo -E LD_LIBRARY_PATH=/opt/libevent2.1/lib swiftfs http://127.0.0.1:8080/auth/v1.0 -v myswiftfs /home/vagrant/mnt`


