libevent_version = '2.1.4-alpha'
default['libevent']['version'] = "#{libevent_version}"
default['libevent']['tarball'] = "http://iweb.dl.sourceforge.net/project/levent/libevent/libevent-2.1/libevent-#{libevent_version}.tar.gz"
default['libevent']['prefix'] = '/opt/libevent2.1/'
