version = node['libevent']['version']
prefix = node['libevent']['prefix']
tarball = node['libevent']['tarball']

remote_file "#{Chef::Config[:file_cache_path]}/libevent-#{version}.tar.gz" do
  source "#{tarball}"
  not_if {::File.exists?("#{prefix}/lib/libevent.a")}
  notifies :run, "script[install-libevent]", :immediately
end

script "install-libevent" do
  interpreter "bash"
  only_if {::File.exists?("#{Chef::Config[:file_cache_path]}/libevent-#{version}.tar.gz")}
  flags "-e -x"
  code <<-EOH
    cd /usr/local/src
    tar xzf #{Chef::Config[:file_cache_path]}/libevent-#{version}.tar.gz
    cd libevent-#{version}
    ./configure --prefix=#{prefix} --enable-openssl --with-pic
    make
    make install
  EOH
end

file "libevent-tarball-cleanup" do
  path "#{Chef::Config[:file_cache_path]}/libevent-#{version}-stable.tar.gz"
  action :delete
end
