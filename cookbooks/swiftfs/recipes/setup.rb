required_packages = [
  "automake", "autoconf", "libtool", "pkg-config", "intltool", "libglib2.0-dev",
  "libfuse-dev", "libxml2-dev", "libssl-dev", "git",
]

extra_packages = node['extra_packages']
(required_packages + extra_packages).each do |pkg|
  package pkg do
    action :install
  end
end
